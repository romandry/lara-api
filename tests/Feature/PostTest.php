<?php

namespace Tests\Feature;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class PostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

//    public function test_some() {
//        //        $post = Post::factory()->make();
//        $post = Post::factory()->raw();
////        $resp = $this->getJson('/api/post/1', ['id'=>1]);
//        $resp = $this->json('GET','/api/post/1', ['id'=>1]);
////        $resp = $this->getJson('/api/post/1', $post);
//        echo '====';print_r($resp);
//        exit;
//    }

    public function test_post_has_db_row() {
//        $post = Post::factory()->raw();
        $post = Post::find(3)->toArray();
        $this->assertDatabaseHas('posts', $post);
    }
    public function test_post_get_one_json_structure() {
        $this->json('GET', '/api/post/1')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'text',
                    'created_at',
                    'updated_at',
                    'age'
                ]
            ]);
    }
    public function test_post_get_all_json_structure() {
        $this->json('GET', '/api/post')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'success',
                'count',
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'text',
                        'age',
                        'thumb' => [
                            'alt',
                            'path'
                        ]
                    ]
                ]
            ]);
    }

    public function test_post_get_one_json() {
        $this->json('GET', '/api/post/1')
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) =>
                $json->has('data', fn($json) =>
                    $json->has('id')
                        ->where('id', 1)
                        ->where('name', 'Post 1')
                        ->etc()));

    }

//    public function test_post_model() {
//        $post = Post::factory()->make();
//
//        $this->get(route('post.show', $post->id))->assertStatus(200);
////        print_r($post);
//    }
}
