<?php


namespace App\Repositories\Interfaces;


interface BrandRepositoryInterface
{
    public function allBrandsWithProducts();
}
