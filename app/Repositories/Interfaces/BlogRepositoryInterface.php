<?php


namespace App\Repositories\Interfaces;


use App\Models\User;

interface BlogRepositoryInterface
{
    public function all();
    public function getByUser(User $user);
    public function deleteBlog(int $blog_id);
    public function createBlog(array $details);
    public function getBlogById(int $blog_id);
    public function updateBlog(int $blog_id, array $details);
}
