<?php


namespace App\Repositories;


use App\Models\Blog;
use App\Models\User;
use App\Repositories\Interfaces\BlogRepositoryInterface;
//use Illuminate\Database\Eloquent\Model;

class BlogRepository implements BlogRepositoryInterface
{

    public function all() {
        return Blog::whereTitle('ttt111')->firstOrFail();
        return Blog::all();
    }

    public function getByUser(User $user) {
        return Blog::where('user_id', $user->id)->get();
    }

    public function deleteBlog(int $blog_id) {
        Blog::destroy($blog_id);
    }

    public function createBlog(array $details) {
        return Blog::create($details);
    }

    public function getBlogById(int $blog_id) {
        return Blog::findOrFail($blog_id);
    }

    public function updateBlog(int $blog_id, array $details) {
        return Blog::whereId($blog_id)->update($details);
    }
}
