<?php

namespace App\Repositories;

use App\Http\Resources\BrandCollection;
use App\Models\Brand;
use App\Repositories\Interfaces\BrandRepositoryInterface;
//use Your Model

/**
 * Class BrandRepository.
 */
class BrandRepository implements BrandRepositoryInterface
{


    public function allBrandsWithProducts() {
        $brands = Brand::with('products')->get();
        return new BrandCollection($brands);
    }
}
