<?php

namespace App\Repositories;

use App\Http\Resources\PostCollection;
use App\Models\Post;
use App\Repositories\Interfaces\PostRepositoryInterface;
//use Your Model

/**
 * Class PostRepository.
 */
class PostRepository implements PostRepositoryInterface
{


    public function all() {
        $posts = Post::with('thumbnail')->get();
        return new PostCollection($posts);
    }
}
