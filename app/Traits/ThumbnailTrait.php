<?php

namespace App\Traits;

use App\Models\Thumbnail;

trait ThumbnailTrait
{
    public function thumbnail() {
        return $this->hasOne(Thumbnail::class);
    }
}
