<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class BlogController extends Controller
{
    private $blogRepository;

    /**
     * BlogController constructor.
     * @param BlogRepositoryInterface $blogRepository
     */
    public function __construct(BlogRepositoryInterface $blogRepository) {
        $this->blogRepository = $blogRepository;
    }

    /**
     * @return mixed
     */
    public function index(): JsonResponse {
        return response()->json($this->blogRepository->all());
//        return response()->json([
//            'data' => $this->blogRepository->all()
//        ]);
    }

    /**
     * @param int $user_id
     * @return mixed
     */
    public function showByUser(int $user_id): JsonResponse {
        $user = User::find($user_id);
        return response()->json([
            'data' => $this->blogRepository->getByUser($user)
        ]);
    }

    public function destroy(int $blog_id): JsonResponse {
        $this->blogRepository->deleteBlog($blog_id);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    public function store(Request $request): JsonResponse {
        $blogDetails = $request->only([
            'title',
            'content'
        ]);
        return response()->json(
            [
                'data' => $this->blogRepository->createBlog($blogDetails)
            ],
            Response::HTTP_CREATED
        );
    }

    public function show(Request $request): JsonResponse {
        $blogId = $request->route('blog');
        return response()->json([
            'data' => $this->blogRepository->getBlogById($blogId)
        ]);
    }

    public function update(Request $request): JsonResponse {
        $blogId = $request->route('blog');
        $blogDetails = $request->only([
            'title',
            'content'
        ]);
        return response()->json([
            'data' => $this->blogRepository->updateBlog($blogId, $blogDetails)
        ]);
    }
}
