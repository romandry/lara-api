<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\Interfaces\PostRepositoryInterface;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository) {
        $this->postRepository = $postRepository;
    }

    public function index() {
        return response()->json($this->postRepository->all());
    }

    /**
     * Show Post by ID
     *
     * @OA\Get(
     *     path="/api/post/{id}",
     *     tags={"Posts"},
     *     description="Get Post by ID",
     *     operationId="training",
     *     security={{"bearer_token": {}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of Post",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int4"
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="Success",
     *       @OA\MediaType(
     *           mediaType="application/json",
     *       )
     *     ),
     *     @OA\Response(response="default", description="Show Post by ID")
     * )
     */
    public function show(int $id): JsonResponse {
        $post = Post::where('id', $id)->first();
        return response()->json(['data' => $post]);
    }
}
