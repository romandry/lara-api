<?php

namespace App\Http\Controllers;

use App\Http\Resources\ThumbinailResource;
use App\Http\Resources\ThumbnailCollection;
use App\Models\Thumbnail;
use Illuminate\Http\Request;

class ThumbnailController extends Controller
{
    static public function getThumbnailByPostId(int $post_id) {
        $thumb = Thumbnail::where('post_id', $post_id)->first();
        return new ThumbinailResource($thumb);
    }
}
