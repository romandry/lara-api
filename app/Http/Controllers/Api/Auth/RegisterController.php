<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use App\Http\Requests\User\RegisterRequest;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Api\Auth
 * @mixin EloquentBuilder
 * @mixin QueryBuilder
 */
class RegisterController extends BaseController
{
    public function register(RegisterRequest $request) {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;

        return $this->sendResponse($success, 'User registered successfully');
    }
}
