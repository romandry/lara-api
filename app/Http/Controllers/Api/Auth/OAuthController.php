<?php

namespace App\Http\Controllers\Api\Auth;

use App\Contracts\AuthTokenGenerator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;

class OAuthController extends BaseController
{
    public const TYPE_PASSWORD = 'password';
    public const TYPE_REFRESH = 'refresh_token';

    private $tokenGenerator;
    // PHP 8 ==> public function __construct(private AuthTokenGenerator $tokenGenerator) {}
    public function __construct(AuthTokenGenerator $tokenGenerator) {
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * Получить ТОКЕН
     *
     * @OA\Post(
     *     path="/api/oauth/token",
     *     tags={"Users"},
     *     operationId="token",
     *     description="Получить ТОКЕН пользователя",
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         description="Email пользователя",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *         description="Password",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ТОКЕН",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     * )
     *
     */
    public function token(Request $request): array
    {
        $request->headers->set('Accept', 'application/json');
        $response = $this->tokenGenerator->generateTokens($this->credentials($request), static::TYPE_PASSWORD);

        if($response->status() !== Response::HTTP_OK) {
            throw ValidationException::withMessages(['email' => 'These credentials do not match our records.']);
        }

        return $response->json();
    }

    public function refresh(Request $request): array
    {
        $request->headers->set('Accept', 'application/json');
        $response = $this->tokenGenerator->generateTokens($request->only('refresh_token'), static::TYPE_REFRESH);

        return $response->json();
    }

    private function credentials(Request $request): array
    {
        return [
            'username' => $request->get('email'),
            'password' => $request->get('password'),
        ];
    }
}
