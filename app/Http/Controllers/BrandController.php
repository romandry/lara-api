<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\BrandRepositoryInterface;

class BrandController extends Controller
{
    private $brandRepository;

    public function __construct(BrandRepositoryInterface $brandRepository) {
        $this->brandRepository = $brandRepository;
    }

    public function index() {
        return response()->json($this->brandRepository->allBrandsWithProducts());
    }
}
