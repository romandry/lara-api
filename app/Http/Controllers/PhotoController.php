<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Http\Request;

/**
 * @Resource("api/photo")
 */
class PhotoController extends Controller
{

    /**
     * @param Photo $photo
     * @return Photo[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Photo $photo) {
        return $photo->all();
    }

    /**
     * @param Request $request
     * @param Photo $photo
     * @return Photo
     */
    public function store(Request $request, Photo $photo) {
        $photo->url = $request->request->get('url');
        $photo->name = $request->request->get('name');
        $photo->save();
        return $photo;
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     */
    public function update(int $id, Request $request) {
        $photo = Photo::find($id);
        $photo->url = $request->request->get('url');
        $photo->name = $request->request->get('name');
        $photo->save();
        return $photo;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id) {
        $photo = Photo::find($id);
        $photo->delete();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id) {
        $photo = Photo::find($id);
        return $photo;
    }
}
