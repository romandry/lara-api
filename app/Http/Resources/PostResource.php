<?php

namespace App\Http\Resources;

use App\Http\Controllers\ThumbnailController;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->resource->id,
            'name'=> $this->resource->name,
            'age'=> $this->resource->age,
            'text'=> $this->resource->text,
            'thumb' => new ThumbinailResource($this->resource->thumbnail)
        ];
    }
}
