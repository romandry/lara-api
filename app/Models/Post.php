<?php

namespace App\Models;

use App\Traits\ThumbnailTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    use ThumbnailTrait;

    protected $fillable = [
        'id',
        'name',
        'text',
        'age'
    ];
}
