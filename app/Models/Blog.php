<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

/**
 * Class Blog
 * @package App\Models
 * @mixin EloquentBuilder
 * @mixin QueryBuilder
 */
class Blog extends Model
{
    use HasFactory;
    protected $fillable = ['content', 'title'];
}
