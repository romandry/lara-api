<?php

use App\Http\Controllers\Api\Auth\OAuthController;
use App\Http\Controllers\Api\Auth\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register','App\Http\Controllers\Api\Auth\RegisterController@register');
Route::resource('brand', 'App\Http\Controllers\BrandController');
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
//Route::resource('photo', 'App\Http\Controllers\PhotoController');
Route::resource('blog', 'App\Http\Controllers\BlogController');
//Route::middleware('auth:api')->group( function() {
    Route::resource('post', 'App\Http\Controllers\PostController');
//});
Route::get('blog/user/{id}', 'App\Http\Controllers\BlogController@showByUser');

Route::group(['prefix' => 'oauth'], function () {
    Route::post('token', [OAuthController::class, 'token'])->name('token');
    Route::post('refresh', [OAuthController::class, 'refresh'])->name('refresh');
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//
//Route::group(['namespace' => 'App\Http\Controllers\Api'], function () {
//    Route::group(['namespace' => 'Auth'], function () {
//        Route::post('register', 'RegisterController');
//        Route::post('login', 'LoginController');
//        Route::post('logout', 'LogoutController')->middleware('auth:api');
//    });
//});
